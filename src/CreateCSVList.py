#! /usr/bin/env python3
# coding:UTF-8

"""
    This is script that download Im@s data with Im@s API 
    and, save the data after shape that.
"""

import GetCharacterData
import setting
import pandas as pd
import sys

# Show status on the console
SHOW_FLAG = False

# receive character data and, return shaped data.
def ShapeDataFrame(chara_list) : 
    save = pd.DataFrame()
    input_parameter = chara_list['input_parameter']
    type_label = chara_list['character_type_label']

    # save a character's data temporary.
    for data in chara_list['character_list'] : 
        tmp = pd.DataFrame()
        tmp['input_parameter'] = [input_parameter]
        tmp['type_label'] = [type_label]

        for params in setting.PARAMS_LIST : 
            try : 
                if params == 'profile_list' : 
                    for params_det in setting.PARAMS_LIST_DET : 
                        try : 
                            detail_data = data[params]
                            # save its params
                            tmp[params_det] = [detail_data[0][params_det]]

                            if SHOW_FLAG : 
                                print('detail : {0} : {1}'\
                                    .format(params_det, detail_data[0][params_det]))

                        except KeyError : 
                            print('{0} does not have {1} data'\
                                    .format(data['name'], params_det))

                else : 
                    # save its params
                    tmp[params] = [data[params]]

                    if SHOW_FLAG : 
                        print(data[params])

            except KeyError : 
                print('{0} does not have {1} data'.format(data['name'], params))
        
        # save the data to dataframe for return.
        #print(tmp)
        save = save.append(tmp)
    
    return save


def Main() : 
    argv = sys.argv
    if len(argv) <= 1 : 
        savename = 'data.csv'

    else : 
        savename = argv[1]

    save = pd.DataFrame()
    num = 0

    while True : 
        chara_list, ret = GetCharacterData.GetCharacterList(num)
        if ret == True : 
            print('Datas that its character_type is {0} have downloaded.'.format(num))
            shape_list = ShapeDataFrame(chara_list)
            print('Downloaded datas have been shaped.')
            save = save.append(shape_list)

        else : 
            break

        num += 1
        
    save.to_csv(setting.SAVE_PASS + savename)
    print('List have been downloaded and shaped complete.')

   

def Test() : 
    save = pd.DataFrame()
    num = 0

    while True : 
        chara_list, ret = GetCharacterData.GetCharacterList(num)
        if ret == True : 
            print('Datas that its character_type is {0} have downloaded.'.format(num))
            shape_list = ShapeDataFrame(chara_list)
            print('Downloaded datas have been shaped.')
            save = save.append(shape_list)

        else : 
            break

        num += 1
        
    save.to_csv('test.csv')
    print('List have been downloaded and shaped complete.')


if __name__ == '__main__' : 
    Main()

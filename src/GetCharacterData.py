#! /usr/bin/env python3
# coding:UTF-8

"""
    アイドルマスターデータ取得APIを用いて、
    任意のデータを取得するスクリプト。
"""

import requests as req
import setting

def GetCharacterData(tp, name) : 
    pass

def GetCharacterList(tp = 1) : 
    param = {
            'type' : str(tp),
            'include_profile' : 'true'
            }

    respons = req.get(setting.CHARA_URL, params = param)

    if respons.status_code == 200 : 
        # このAPIは、データが存在しない場合'result'の'code'に400を返す。
        result = respons.json()['result']

        if result['code'] == 200 : 
            return respons.json(), True

        else : 
            return None, False

    else : 
        return None, False

if __name__ == "__main__" : 
    chara_list, ret = GetCharacterList(50)
    print(chara_list)
    print(ret)

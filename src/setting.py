# coding:UTF-8

"""
    各種定数など
"""

CHARA_URL = 'http://api.imas-db.jp/character/list'
PARAMS_LIST = ('character_type', 'origin_media', 'id', 'class_name', \
        'name', 'name_ruby', 'first_name', 'first_name_ruby', 'family_name', \
        'family_name_ruby', 'cv', 'gender', 'birth_month', 'birth_day', \
        'arrival_date', 'profile_list', 'is_idol', 'is_foreigner_name')

PARAMS_LIST_DET = ('age', 'generation', 'height', 'weight', \
        'bust', 'waist', 'hip', 'blood_type', 'hometown', 'dominant_hand', \
        'hobby', 'favorite', 'specialty', 'memo')

LIST_LABEL = ('input_parameter', 'character_type_label', 'character_list')

SAVE_PASS = '../data/'
